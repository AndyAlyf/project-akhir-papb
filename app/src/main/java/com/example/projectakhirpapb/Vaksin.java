package com.example.projectakhirpapb;

import java.sql.Timestamp;
import java.util.Date;

public class Vaksin {
    private String id_vaksin;
    private String asal_negara;
    private String efikasi;
    private String nama;


    public String getId_vaksin(){

        return id_vaksin;
    }

    public void setId_vaksin(String id_vaksin){

        this.id_vaksin= id_vaksin;
    }

    public String getAsal_negara() {

        return asal_negara;
    }

    public void setAsal_negara(String asal_negara) {

        this.asal_negara = asal_negara;
    }

    public String getEfikasi() {

        return efikasi;
    }

    public void setEfikasi(String efikasi) {

        this.efikasi = efikasi;
    }

    public String getNama() {

        return nama;
    }

    public void setNama(String nama) {

        this.nama = nama;
    }
}


