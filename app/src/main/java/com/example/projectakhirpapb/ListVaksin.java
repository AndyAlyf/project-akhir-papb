package com.example.projectakhirpapb;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

public class ListVaksin extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    TextView tvNama;
    Button btLogout;
    RecyclerView rv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaksin);
        tvNama = findViewById(R.id.tvVaksin);
        btLogout = findViewById(R.id.btLogout);

        firebaseAuth = FirebaseAuth.getInstance();
        rv = findViewById(R.id.rvVaksin);
        Query query = FirebaseFirestore.getInstance()
                .collection("vaksin")
                .orderBy("nama")
                .limit(50);
        FirestoreRecyclerOptions<Vaksin> options = new
                FirestoreRecyclerOptions.Builder<Vaksin>()
                .setQuery(query, Vaksin.class)
                .build();

        FirestoreRecyclerAdapter<Vaksin, VaksinViewHolder> adapter = new FirestoreRecyclerAdapter<Vaksin, VaksinViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull VaksinViewHolder holder, int position, @NonNull Vaksin vaksin) {

                holder.setIsi(position, vaksin);
            }
            @NonNull
            @Override
            public VaksinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                Log.d("LIST","onCreateViewHolder");
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.table_list_vaksin, parent, false);
                return new VaksinViewHolder(view);
            }
        };
        adapter.startListening();
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));

        Query query1 = FirebaseFirestore.getInstance().collection("akun").whereEqualTo("id_user", firebaseAuth.getUid());
        query1.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                String nama = task.getResult().getDocuments().get(0).getData().get("nama").toString();
                tvNama.setText(nama);
            }
        });
        btLogout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {
                Log.d("testing", "logout");
                firebaseAuth.signOut();
                startActivity(new Intent(ListVaksin.this, MainActivity.class));
            }
        });
    }

    public class VaksinViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView tvIdVaksin, tvNamaVaksin, tvAsalNegara, tvEfikasi;
        Button btdetail;

        public VaksinViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
        }
        public void setIsi(int position, Vaksin vaksin) {
            tvNamaVaksin = view.findViewById(R.id.nama);
            tvAsalNegara = view.findViewById(R.id.asal_negara);
            tvEfikasi = view.findViewById(R.id.efikasi);
            btdetail = view.findViewById(R.id.detail);
            btdetail.setText("Detail");

            tvNamaVaksin.setBackgroundResource(R.drawable.table_content_cell_bg);
            tvAsalNegara.setBackgroundResource(R.drawable.table_content_cell_bg);
            tvEfikasi.setBackgroundResource(R.drawable.table_content_cell_bg);
//            btdetail.setBackgroundResource(R.drawable.table_content_cell_bg);

            tvNamaVaksin.setText(vaksin.getNama());
            tvAsalNegara.setText(vaksin.getAsal_negara());
            tvEfikasi.setText(vaksin.getEfikasi());
            final String[] id = {""};
            Intent intent = new Intent(ListVaksin.this, ListReviewVaksin.class);
            FirebaseFirestore.getInstance().collection("vaksin").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    id[0] = task.getResult().getDocuments().get(position).getId();
                    intent.putExtra("id_vaksin", id[0]);
                }
            });
            intent.putExtra("nama_vaksin", vaksin.getNama());
            btdetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(intent);
                }
            });
        }


    }
}
