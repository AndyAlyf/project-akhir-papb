package com.example.projectakhirpapb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ListReviewVaksin extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    TextView tvVaksin;
    Button btAdd;
    RecyclerView rv;
    String id_curent_user;
    boolean sudah_review = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_review_vaksin);
        tvVaksin = findViewById(R.id.tvVaksin);
        btAdd = findViewById(R.id.btAdd);
        rv = findViewById(R.id.rvReview);
        firebaseAuth = FirebaseAuth.getInstance();
        id_curent_user = firebaseAuth.getCurrentUser().getUid();

        Intent intent = getIntent();
        tvVaksin.setText(intent.getStringExtra("nama_vaksin"));

        Query query = FirebaseFirestore.getInstance()
                .collection("review_vaksin")
                .whereEqualTo("id_vaksin", intent.getStringExtra("id_vaksin"))
                .orderBy("tanggal_review")
                .limit(50);
        FirestoreRecyclerOptions<Review> options = new
                FirestoreRecyclerOptions.Builder<Review>()
                    .setQuery(query, Review.class)
                    .build();

        FirestoreRecyclerAdapter<Review, ReviewViewHolder> adapter = new FirestoreRecyclerAdapter<Review, ReviewViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ReviewViewHolder holder, int position, @NonNull Review review) {
                holder.setIsi(position, review);
            }

            @NonNull
            @Override
            public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                Log.d("LIST","onCreateViewHolder");
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row, parent, false);
                return new ReviewViewHolder(view);
            }
        };
        adapter.startListening();
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));

        FirebaseFirestore.getInstance()
                .collection("review_vaksin")
                .whereEqualTo("id_user",id_curent_user)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (QueryDocumentSnapshot document : task.getResult()){
                                if(document.get("id_user").equals(id_curent_user)){
                                    sudah_review = true;
                                    break;
                                }
                            }
                        };
                        if (sudah_review){
                            btAdd.setText("Edit Review");
                            btAdd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Log.d("testing", "add review");
                                    startActivity(new Intent(ListReviewVaksin.this,updateReview.class));
                                }
                            });
                        } else {
                            btAdd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Log.d("testing", "add review");
                                    startActivity(new Intent(ListReviewVaksin.this,AddReview.class));
                                }
                            });
                        }
                    }
                });
    }

    public static class ReviewViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView tvNomor, tvNamaRow, tvTanggal, tvReview;

        public ReviewViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
        }

        public void setIsi(int position, Review review) {
            tvNomor = view.findViewById(R.id.tvNomor);
            tvNamaRow = view.findViewById(R.id.tvNamaRow);
            tvTanggal = view.findViewById(R.id.tvTanggal);
            tvReview = view.findViewById(R.id.tvReview);
            tvNomor.setBackgroundResource(R.drawable.table_content_cell_bg);
            tvNamaRow.setBackgroundResource(R.drawable.table_content_cell_bg);
            tvTanggal.setBackgroundResource(R.drawable.table_content_cell_bg);
            tvReview.setBackgroundResource(R.drawable.table_content_cell_bg);
            String no = position + 1 + "";
            tvNomor.setText(no);
            FirebaseFirestore.getInstance().collection("akun").whereEqualTo("id_user", review.getId_user()).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            String nama = task.getResult().getDocuments().get(0).getData().get("nama").toString();
                            tvNamaRow.setText(nama);
                        }
                    });
            tvTanggal.setText(review.getTanggal_review().toString());
            tvReview.setText(review.getIsi_review());

        }
    }
}