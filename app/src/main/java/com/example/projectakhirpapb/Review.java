package com.example.projectakhirpapb;

import com.google.firebase.Timestamp;

import java.util.Date;

public class Review {
    private String id_user;
    private String id_vaksin;
    private String isi_review;
    private Date tanggal_review;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_vaksin() {
        return id_vaksin;
    }

    public void setId_vaksin(String id_vaksin) {
        this.id_vaksin = id_vaksin;
    }

    public String getIsi_review() {
        return isi_review;
    }

    public void setIsi_review(String isi_review) {
        this.isi_review = isi_review;
    }

    public Date getTanggal_review() {
        return tanggal_review;
    }

    public void setTanggal_review(Date tanggal_review) {
        this.tanggal_review = tanggal_review;
    }
}
