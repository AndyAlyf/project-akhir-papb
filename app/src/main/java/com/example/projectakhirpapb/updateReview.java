package com.example.projectakhirpapb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.sql.SQLOutput;
import java.sql.Timestamp;
import java.util.*;

public class updateReview extends AppCompatActivity {

    Spinner sp;
    Button submitButton;
    TextView review;
    String id_vaksin_exist,id_vaksin,uid,id_review,nama_vaksin, nama_vaksin_exist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_review);

        sp = (Spinner) findViewById(R.id.spinner2);
        submitButton = findViewById(R.id.submitButton);
        review = findViewById(R.id.review);
        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        db.collection("review_vaksin")
                .whereEqualTo("id_user",uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if  (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()){
                                Log.d("vaksin_review",document.getId() + " => " + document.getData());
                                review.setText(document.get("isi_review").toString());
                                id_review = document.getId();
                                id_vaksin_exist = document.get("id_vaksin").toString();
                                Log.d("ID_VAKSIN",  id_vaksin_exist);
                            }
                        }
                    }
                });

        List<String> arrList = new ArrayList<>();
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, arrList);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adp1);

        db.collection("vaksin")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (QueryDocumentSnapshot document : task.getResult()){
                                Log.d("VAKSIN", document.getId() + " => " + document.getData());
                                if (id_vaksin_exist.equals(document.getId())){
                                    nama_vaksin_exist = document.get("nama").toString();
                                    Log.d("NAMA_VAKSIN_EXIST", nama_vaksin_exist);

                                }
                                arrList.add(document.get("nama").toString());
                            }
                            adp1.notifyDataSetChanged();
                            if (nama_vaksin_exist != null) {
                                int spinnerPosition = adp1.getPosition(nama_vaksin_exist);
                                sp.setSelection(spinnerPosition);
                            }
                        } else {
                            Log.d("Error","Error getting documents",task.getException());
                        }
                    }
                });

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                nama_vaksin = adapterView.getItemAtPosition(i).toString();
                db.collection("vaksin")
                        .whereEqualTo("nama",nama_vaksin)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()){
                                    for (QueryDocumentSnapshot document : task.getResult()){
                                        id_vaksin = document.getId().toString();
                                        Log.d("Vaksin_id", "id vaksin"+id_vaksin);
                                    }
                                } else {
                                    Log.d("Vaksin_id","Error getting ",task.getException());
                                }
                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, Object> data = new HashMap<>();
                data.put("id_user",uid);
                data.put("id_vaksin",id_vaksin);
                data.put("isi_review",review.getText().toString());
                data.put("tanggal_review",new Timestamp(System.currentTimeMillis()));

                db.collection("review_vaksin")
                        .document(id_review)
                        .set(data)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                Log.d("OVERWRITE", "DocumentSnapshot successfully written!");
                                startActivity(new Intent(updateReview.this, ListVaksin.class));
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d("OVERWRITE", "error",e);
                            }
                        });
            }
        });
    };


}